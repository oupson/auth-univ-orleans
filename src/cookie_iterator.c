#include <auth_univ_orleans/cookie_iterator.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>

#define STR_BUF_SIZE 256

cookie_iterator_t *new_cookie_iterator_from_curl(CURL *handle) {
    struct cookie_iterator *iterator =
        (struct cookie_iterator *)calloc(1, sizeof(struct cookie_iterator));
    iterator->cookie_list = NULL;
    int res =
        curl_easy_getinfo(handle, CURLINFO_COOKIELIST, &iterator->cookie_list);

    if (!res && iterator->cookie_list) {
        iterator->current = iterator->cookie_list;
        iterator->cookie = (struct cookie *)calloc(1, sizeof(struct cookie));

        return iterator;
    } else {
        return NULL;
    }
}

cookie_t *cookie_iterator_next(cookie_iterator_t *iterator) {
    if (iterator->cookie_list == NULL) {
        return NULL;
    }

    bool have_value = false;

    if (iterator->current != NULL) {
        have_value = true;
        char *data = iterator->current->data;
        char buf[STR_BUF_SIZE];

        int offset = 0;

        char c;
        int i = 0;
        while ((c = data[i]) && c != '\t' &&
               i < sizeof(iterator->cookie->hostname) - 1) {
            iterator->cookie->hostname[i] = c;
            i++;
        }
        iterator->cookie->hostname[i] = '\0';
        i++;
        offset += i;

        i = 0;
        while ((c = data[offset + i]) && c != '\t' && i < STR_BUF_SIZE) {
            buf[i] = c;
            i++;
        }
        buf[i] = '\0';
        i++;
        offset += i;

        if (strncmp(buf, "TRUE", 4) == 0) {
            iterator->cookie->include_subdomains = 1;
        } else {
            iterator->cookie->include_subdomains = 0;
        }

        i = 0;
        while ((c = data[offset + i]) && c != '\t' &&
               i < sizeof(iterator->cookie->path) - 1) {
            iterator->cookie->path[i] = c;
            i++;
        }
        iterator->cookie->path[i] = '\0';
        i++;
        offset += i;

        i = 0;
        while ((c = data[offset + i]) && c != '\t' && i < STR_BUF_SIZE) {
            buf[i] = c;
            i++;
        }
        buf[i] = '\0';
        i++;
        offset += i;

        if (strncmp(buf, "TRUE", 4) == 0) {
            iterator->cookie->secure = 1;
        } else {
            iterator->cookie->secure = 0;
        }

        i = 0;
        while ((c = data[offset + i]) && c != '\t' && i < STR_BUF_SIZE) {
            buf[i] = c;
            i++;
        }
        buf[i] = '\0';
        i++;
        offset += i;

        iterator->cookie->expire = atol(buf);

        i = 0;
        while ((c = data[offset + i]) && c != '\t' &&
               i < sizeof(iterator->cookie->name) - 1) {
            iterator->cookie->name[i] = c;
            i++;
        }
        iterator->cookie->name[i] = '\0';
        i++;
        offset += i;

        i = 0;
        while ((c = data[offset + i]) && c != '\t' &&
               i < sizeof(iterator->cookie->value) - 1) {
            iterator->cookie->value[i] = c;
            i++;
        }
        iterator->cookie->value[i] = '\0';

        iterator->current = iterator->current->next;
    }

    return (have_value) ? iterator->cookie : NULL;
}

void cookie_iterator_free(cookie_iterator_t *iterator) {
    if (iterator->cookie_list != NULL) {
        curl_slist_free_all(iterator->cookie_list);
        iterator->cookie_list = NULL;
    }

    if (iterator->cookie != NULL) {
        cookie_free(iterator->cookie);
        iterator->cookie = NULL;
    }

    iterator->current = NULL;

    free(iterator);
}

char *cookie_hostname(cookie_t *cookie) { return cookie->hostname; }
bool cookie_include_subdomains(cookie_t *cookie) {
    return cookie->include_subdomains;
}
char *cookie_path(cookie_t *cookie) { return cookie->path; }
bool cookie_secure(cookie_t *cookie) { return cookie->secure; }
long cookie_expire(cookie_t *cookie) { return cookie->expire; }
char *cookie_name(cookie_t *cookie) { return cookie->name; }
char *cookie_value(cookie_t *cookie) { return cookie->value; }

void cookie_free(cookie_t *cookie) { free(cookie); }
