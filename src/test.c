#include <auth_univ_orleans/auth_univ_orleans.h>
#include <auth_univ_orleans/cookie_iterator.h>
#include <stdlib.h>

int main(void) {
    char *username = getenv("USERNAME");
    char *password = getenv("PASSWORD");

    if (username == NULL || password == NULL) {
        fprintf(stderr, "ERROR : missing USERNAME or PASSWORD env var\n");
        return -1;
    }

    result_t *res = auth_univ_get_session_token(username, password);

    if (get_result_type(res) == OK) {

        cookie_iterator_t *iter = get_result_ok(res);
        cookie_t *cookie;

        while ((cookie = cookie_iterator_next(iter))) {
            printf("%s :\n"
                   "\thostname: %s\n"
                   "\tinclude subdomains : %d\n"
                   "\tpath : %s\n"
                   "\tsecure : %d\n"
                   "\texpire : %ld\n"
                   "\tvalue: %s\n",
                   cookie_name(cookie), cookie_hostname(cookie),
                   cookie_include_subdomains(cookie), cookie_path(cookie),
                   cookie_secure(cookie), cookie_expire(cookie),
                   cookie_value(cookie));
        }

    } else {
        fprintf(stderr, "error : %s\n", get_result_err(res));
    }

    result_free(res);
}
