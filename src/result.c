#include <auth_univ_orleans/cookie_iterator.h>
#include <auth_univ_orleans/result.h>
#include <stdlib.h>

enum result_type get_result_type(result_t *result) { return result->type; }

cookie_iterator_t *get_result_ok(result_t *result) { return result->inner.ok; }

cookie_iterator_t *move_result_ok(result_t *result) {
    cookie_iterator_t *res = result->inner.ok;
    result->inner.ok = NULL;
    return res;
}

const char *get_result_err(result_t *result) { return result->inner.error; }

void result_free(result_t *result) {
    if (result->type == OK) {
        if (result->inner.ok != NULL) {
            cookie_iterator_free(result->inner.ok);
            result->inner.ok = NULL;
        }
    } else {
        free(result->inner.error);
        result->inner.error = NULL;
    }
    free(result);
}
