#include <auth_univ_orleans/auth_univ_orleans.h>
#include <auth_univ_orleans/cookie_iterator.h>
#include <auth_univ_orleans/result.h>
#include <curl/curl.h>
#include <stdlib.h>
#include <string.h>
#include <tidy/tidy.h>
#include <tidy/tidybuffio.h>

int __auth_univ_orleans_post_login(CURL *handle, char *execution, char *action,
                                   char *username, char *password);

int __get_action_and_execution(CURL *handle, char **execution, char **action);

size_t __dumb_write_callback(void *data, size_t size, size_t nmemb,
                             void *userp) {
    return size * nmemb;
}

size_t __tidy_buf_write_cb(void *in, size_t size, size_t nmemb, void *out) {
    size_t r;
    r = size * nmemb;
    tidyBufAppend((TidyBuffer *)out, in, r);
    return r;
}

int __get_action_and_execution_from_node(TidyDoc doc, TidyNode tnod,
                                         char **execution, char **action);

result_t *auth_univ_get_session_token(char *username, char *password) {
    CURL *handle = curl_easy_init();
    char errbuf[CURL_ERROR_SIZE];

    curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, errbuf);
    curl_easy_setopt(handle, CURLOPT_TCP_KEEPALIVE, 1L);

    /* set the error buffer as empty before performing a request */
    errbuf[0] = 0;

    // curl_easy_setopt(handle, CURLOPT_NOPROGRESS, 1L);

    int res = auth_univ_orleans_login(handle, username, password);

    if (res == CURLE_OK) {
        cookie_iterator_t *list = new_cookie_iterator_from_curl(handle);

        curl_easy_cleanup(handle);

        struct result *res = (struct result *)calloc(1, sizeof(struct result));
        res->type = OK;
        res->inner.ok = list;

        return res;
    } else {
        struct result *res = (struct result *)calloc(1, sizeof(struct result));
        res->type = ERR;
        res->inner.error = strdup(errbuf);
        curl_easy_cleanup(handle);
        return res;
    }
}

int auth_univ_orleans_login(CURL *handle, char *username, char *password) {
    int res = 0;
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_COOKIEFILE, "");
    curl_easy_setopt(handle, CURLOPT_USERAGENT,
                     "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:93.0) "
                     "Gecko/20100101 Firefox/93.0");
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, __dumb_write_callback);

    curl_easy_setopt(handle, CURLOPT_URL, "https://ent.univ-orleans.fr/");

    res = curl_easy_perform(handle);

    if (res == CURLE_OK) {
        char *execution = NULL;
        char *action = NULL;
        res = __get_action_and_execution(handle, &execution, &action);

        if (res == CURLE_OK) {
            if (execution == NULL || action == NULL) {
                return 0;
            }

            curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION,
                             __dumb_write_callback);

            res = __auth_univ_orleans_post_login(handle, execution, action,
                                                 username, password);

            free(execution);
            free(action);
            return res;
        } else {
            return res;
        }
    } else {
        return res;
    }

    return 0;
}

int __get_action_and_execution(CURL *handle, char **execution, char **action) {
    TidyDoc tdoc;
    TidyBuffer docbuf = {0};
    // TidyBuffer tidy_errbuf = {0};

    int err;
    curl_easy_setopt(
        handle, CURLOPT_URL,
        "https://auth.univ-orleans.fr/cas/"
        "login?service=https%3A%2F%2Fnotes-iuto.univ-orleans.fr%2F");
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, __tidy_buf_write_cb);

    tdoc = tidyCreate();
    tidyOptSetBool(tdoc, TidyShowWarnings, no);
    tidyOptSetInt(tdoc, TidyWrapLen, 4096);
    tidyBufInit(&docbuf);
    tidyOptSetBool(tdoc, TidyQuiet, 1L);

    curl_easy_setopt(handle, CURLOPT_WRITEDATA, &docbuf);

    err = curl_easy_perform(handle);
    if (err == CURLE_OK) {
        err = tidyParseBuffer(tdoc, &docbuf);
        /* parse the input */ // TODO ERROR
        if (err >= 0) {
            err = __get_action_and_execution_from_node(
                tdoc, tidyGetRoot(tdoc), execution, action); /* walk the tree */
        }
    }

    tidyBufFree(&docbuf);
    // tidyBufFree(&tidy_errbuf);
    tidyRelease(tdoc);
    return err;
}

int __auth_univ_orleans_post_login(CURL *handle, char *execution, char *action,
                                   char *username, char *password) {
    char *encoded_execution =
        curl_easy_escape(handle, execution, strlen(execution));
    char *encoded_username =
        curl_easy_escape(handle, username, strlen(username));
    char *encoded_password =
        curl_easy_escape(handle, password, strlen(password));

    size_t form_data_size =
        strlen(encoded_execution) + strlen(encoded_username) +
        strlen(encoded_password) +
        sizeof("username=&password=&_eventId=submit&execution=");
    char *form_data = (char *)malloc(form_data_size * sizeof(char));

    int nbr = snprintf(form_data, form_data_size,
                       "username=%s&password=%s&_eventId=submit&execution=%s",
                       encoded_username, encoded_password, encoded_execution);

    curl_easy_setopt(handle, CURLOPT_POSTFIELDS, form_data);
    curl_easy_setopt(handle, CURLOPT_POSTFIELDSIZE, nbr);

    curl_easy_setopt(handle, CURLOPT_POST, 1L);

    /*
        int action_size = strlen(action);
      char *url = (char *)calloc(33 + action_size + 1, sizeof(char));

      memcpy(url, "https://auth.univ-orleans.fr/cas/", 33);
      memcpy(url + 33, action, action_size);
      */

    curl_easy_setopt(
        handle, CURLOPT_URL, /* url*/
        "https://auth.univ-orleans.fr/cas/"
        "login?service=https%3A%2F%2Fnotes-iuto.univ-orleans.fr%2F");

    int res = curl_easy_perform(handle);

    // free(url);

    curl_free(encoded_execution);
    curl_free(encoded_username);
    curl_free(encoded_password);

    free(form_data);

    return res;
}

int __get_action_and_execution_from_node(TidyDoc doc, TidyNode tnod,
                                         char **execution, char **action) {
    TidyNode child;
    for (child = tidyGetChild(tnod); child; child = tidyGetNext(child)) {
        ctmbstr name = tidyNodeGetName(child);
        if (name) {
            TidyAttr attr;
            int is_execution = 0;
            int is_action = 0;
            const char *value = NULL;

            for (attr = tidyAttrFirst(child); attr; attr = tidyAttrNext(attr)) {
                if (!is_execution && !is_action &&
                    strcmp(tidyAttrName(attr), "name") == 0 &&
                    strcmp(tidyAttrValue(attr), "execution") ==
                        0) // TODO OPTIMISE
                {
                    is_execution = 1;
                } else if (!is_action && !is_execution &&
                           strcmp(tidyAttrName(attr), "id") == 0 &&
                           strcmp(tidyAttrValue(attr), "fm1") ==
                               0) // TODO OPTIOMISE
                {
                    is_action = 1;
                } else if (strcmp(tidyAttrName(attr), "value") == 0) {
                    value = tidyAttrValue(attr);
                } else if (!is_execution &&
                           strcmp(tidyAttrName(attr), "action") == 0) {
                    value = tidyAttrValue(attr);
                }
            }

            if (is_execution && value != NULL) {
                *execution = strdup(value);
            } else if (is_action && value != NULL) {
                *action = strdup(value);
            }
        }

        if (*execution == NULL || *action == NULL) {
            if (__get_action_and_execution_from_node(doc, child, execution,
                                                     action) == 0) {
                return 0;
            }
        }
    }
    return (*execution != NULL && *action != NULL) ? 0 : -1;
}
