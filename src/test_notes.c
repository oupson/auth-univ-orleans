#include <auth_univ_orleans/auth_univ_orleans.h>
#include <auth_univ_orleans/cookie_iterator.h>
#include <stdlib.h>

size_t writeFunction(void *ptr, size_t size, size_t nmemb, void *data) {
    fwrite(ptr, nmemb, size, stdout);
    return size * nmemb;
}

int main(void) {
    char *username = getenv("USERNAME");
    char *password = getenv("PASSWORD");

    if (username == NULL || password == NULL) {
        fprintf(stderr, "ERROR : missing USERNAME or PASSWORD env var\n");
        return -1;
    }

    CURL *handle = curl_easy_init();
    char errbuf[CURL_ERROR_SIZE];

    curl_easy_setopt(handle, CURLOPT_ERRORBUFFER, errbuf);
    curl_easy_setopt(handle, CURLOPT_TCP_KEEPALIVE, 1L);

    int res = auth_univ_orleans_login(handle, username, password);

    if (res != CURLE_OK) {
        fprintf(stderr, "error : %s\n", errbuf);
        return res;
    }

    curl_easy_setopt(handle, CURLOPT_HTTPGET, 1L);

    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, writeFunction);

    curl_easy_perform(handle);
    curl_easy_cleanup(handle);
}
