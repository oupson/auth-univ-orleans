#ifndef AUTH_UNIV_ORLEANS
#define AUTH_UNIV_ORLEANS

#include <auth_univ_orleans/cookie_iterator.h>
#include <auth_univ_orleans/result.h>
#include <curl/curl.h>

result_t *auth_univ_get_session_token(char *username, char *password);
int auth_univ_orleans_login(CURL *handle, char *username, char *password);
#endif