#ifndef ERROR_H
#define ERROR_H

#include "auth_univ_orleans/cookie_iterator.h"
typedef struct result result_t;

#include <auth_univ_orleans/auth_univ_orleans.h>

enum result_type {
    OK = 0,
    ERR = 1,
};

union inner_result {
    cookie_iterator_t *ok;
    char *error;
};

struct result {
    enum result_type type;
    union inner_result inner;
};

enum result_type get_result_type(result_t *result);
cookie_iterator_t *get_result_ok(result_t *result);
cookie_iterator_t *move_result_ok(result_t *result);
const char *get_result_err(result_t *result);
void result_free(result_t *result);
#endif
