#ifndef COOKIE_ITERATOR_H
#define COOKIE_ITERATOR_H

#include <curl/curl.h>
#include <stdbool.h>

struct cookie_iterator {
    struct curl_slist *cookie_list;
    struct curl_slist *current;
    struct cookie *cookie;
};

struct cookie {
    char hostname[256];
    bool include_subdomains;
    char path[256];
    bool secure;
    long expire;
    char name[256];
    char value[256];
};

typedef struct cookie_iterator cookie_iterator_t;
typedef struct cookie cookie_t;

cookie_iterator_t *new_cookie_iterator_from_curl(CURL *handle);
cookie_t *cookie_iterator_next(cookie_iterator_t *iterator);
void cookie_iterator_free(cookie_iterator_t *iterator);

char *cookie_hostname(cookie_t *cookie);
bool cookie_include_subdomains(cookie_t *cookie);
char *cookie_path(cookie_t *cookie);
bool cookie_secure(cookie_t *cookie);
long cookie_expire(cookie_t *cookie);
char *cookie_name(cookie_t *cookie);
char *cookie_value(cookie_t *cookie);
void cookie_free(cookie_t *cookie);

#endif
