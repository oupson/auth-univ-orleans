.PHONY: all clean format

all: lib/libauthunivorleans.so lib/libauthunivorleans.a bin/test bin/test_notes

CFLAGS += -Iinclude $(DEFINES) `pkg-config --cflags libcurl` -fPIC -Wall -Werror

FLAGS = -g

SRCS=$(wildcard src/*.c)
OBJS=$(SRCS:src/%.c=obj/%.o)
DEPS = $(OBJS:%.o=%.d)

-include $(DEPS)

clean:
	rm -fr bin/* obj/* lib/*

format:
	clang-format src/*.c -i
	clang-format include/**/*.h -i

CC ?= $(CC)

obj/%.o: src/%.c
	@mkdir -p $(@D)
	$(CC) -c -MMD -o $@ $< $(CFLAGS) $(FLAGS)

lib/libauthunivorleans.so: obj/auth_univ_orleans.o obj/cookie_iterator.o obj/result.o
	@mkdir -p $(@D)
	$(CC) -fpic -shared $(CFLAGS) $(FLAGS) -o $@ $^ `pkg-config --libs libcurl` `pkg-config --libs tidy`

lib/libauthunivorleans.a: obj/auth_univ_orleans.o obj/cookie_iterator.o obj/result.o
	@mkdir -p $(@D)
	$(AR) -cr $@ $^

bin/test: obj/test.o lib/libauthunivorleans.a
	@mkdir -p $(@D)
	$(CC) -o $@ $^ $(CFLAGS) `pkg-config --libs libcurl` `pkg-config --libs tidy`

bin/test_notes: obj/test_notes.o lib/libauthunivorleans.a
	@mkdir -p $(@D)
	$(CC) -o $@ $^ $(CFLAGS) `pkg-config --libs libcurl` `pkg-config --libs tidy`